FROM registry.gitlab.com/mostlybroken/puuhapete/ubuntu-focal:latest as builder

WORKDIR /build
COPY fakevagrant /build/fakevagrant
RUN chmod -R 755 fakevagrant/DEBIAN
RUN dpkg-deb --build fakevagrant


FROM registry.gitlab.com/mostlybroken/androiden/temurin-base:8

COPY fdroidserver.list /etc/apt/sources.list.d/
COPY fdroidserver.asc /etc/apt/trusted.gpg.d/

COPY --from=builder /build/fakevagrant.deb /tmp/fakevagrant.deb
RUN apt-get install /tmp/fakevagrant.deb \
      && rm /tmp/fakevagrant.deb

RUN apt-get update \
      && apt-get upgrade -y \
      && DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends openssh-client fdroidserver \
      && apt-get autoremove --purge \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*

RUN UNPACK_DIR="/opt/android-sdk/cmdline-tools/latest"; \
      mkdir -p ${UNPACK_DIR} \
      && bsdtar -xvf /sdk-archive/commandlinetools-linux.zip --strip-components=1 -C ${UNPACK_DIR} \
      && yes | /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager --licenses \
      && /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager --update \
      && /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager --install "build-tools;31.0.0"
